﻿<%@ Page Title="Portfolio" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BackEnd.aspx.cs" Inherits="stylepractice.BackEnd" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
        <h3>Back End Projects</h3>
    Here are a list of project where I was involved in database development, interfaces for content management systems, applying CRUD architecture.
</asp:Content>

<asp:Content ContentPlaceHolderID="SideBar" runat="server">
    <uctrl:contact runat="server" id="contact_sidebar" />
</asp:Content>