﻿<%@ Page Title="Portfolio" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FrontEnd.aspx.cs" Inherits="stylepractice.FrontEnd" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h3>Front End</h3>
    <p>Here are the projects where I contributed to the visual layout, UI, or accessibility options.</p>
</asp:Content>

<asp:Content ContentPlaceHolderID="SideBar" runat="server">
    <uctrl:contact runat="server" id="contact_sidebar" />
</asp:Content>
