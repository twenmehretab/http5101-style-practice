﻿<%@ Page Title="Experience" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Experience.aspx.cs" Inherits="stylepractice.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>

    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item item active">
          <img class="d-block w-100" src="images/campus1.jpg" alt="First slide">
        </div>
        <div class="carousel-item item">
          <img class="d-block w-100" src="images/campus2.jpg" alt="Second slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <h3>Education</h3>
    <ul>
        <li>BA arts, College of North Dakota University</li>
        <li>Certificate of Web Design, Iowa</li>
    </ul>
    <h3>Experience</h3>
    <ul>
        <li>Lead front end designer at whubberjubs, 6 months</li>
        <li>Project Manager at Boxchatter, 1 year</li>
        <li>Senior developer at Knitters Weave</li>
    </ul>
</asp:Content>

<asp:Content ContentPlaceHolderID="SideBar" runat="server">
    <uctrl:contact runat="server" id="contact_sidebar" />
</asp:Content>