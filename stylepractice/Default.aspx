﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="stylepractice._Default" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div id="body">
        <div class="jumbotron">
            <h1>Hi there, I'm Jane Doe</h1>
            <p class="lead">Web Developer, Designer, Manager.</p>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h2>Scalable Solutions</h2>
                <p>
                    My web application solutions are a perfect fit for every business, large or small. You can be confident that your site will grow with you.
                </p>
                
            </div>
            <div class="col-md-6">
                <h2>Measurable Results</h2>
                <p>
                    Generate a spectrum of reports on the fly. Get to know your user demographic, witness the impact of SEO, or tabulate your online revenue.
                </p>
                
            </div>
            <div class="col-md-6">
                <h2>Advanced Web Applications</h2>
                <p>
                    With a data-driven site, your customers can interact with your business 24/7.
                </p>
                
            </div>
            <div class="col-md-6">
                <h2>Designed for mobile and desktop</h2>
                <p>
                    With CSS and bootstrap responsive designs, your site will look great on any device.
                </p>
                
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="SideBar" runat="server">
    <uctrl:contact runat="server" id="contact_sidebar" />
</asp:Content>